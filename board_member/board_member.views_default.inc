<?php
/**
 * @file
 * board_member.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function board_member_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'board_members';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Board members';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Board members';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_bm_term' => 'field_bm_term',
    'field_bm_email' => 'field_bm_email',
    'field_bm_twitter' => 'field_bm_twitter',
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no board members posted.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Content: Photo */
  $handler->display->display_options['fields']['field_bm_photo']['id'] = 'field_bm_photo';
  $handler->display->display_options['fields']['field_bm_photo']['table'] = 'field_data_field_bm_photo';
  $handler->display->display_options['fields']['field_bm_photo']['field'] = 'field_bm_photo';
  $handler->display->display_options['fields']['field_bm_photo']['label'] = '';
  $handler->display->display_options['fields']['field_bm_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_bm_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_bm_photo']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Position */
  $handler->display->display_options['fields']['field_bm_position']['id'] = 'field_bm_position';
  $handler->display->display_options['fields']['field_bm_position']['table'] = 'field_data_field_bm_position';
  $handler->display->display_options['fields']['field_bm_position']['field'] = 'field_bm_position';
  $handler->display->display_options['fields']['field_bm_position']['label'] = '';
  $handler->display->display_options['fields']['field_bm_position']['element_label_colon'] = FALSE;
  /* Field: Content: Term */
  $handler->display->display_options['fields']['field_bm_term']['id'] = 'field_bm_term';
  $handler->display->display_options['fields']['field_bm_term']['table'] = 'field_data_field_bm_term';
  $handler->display->display_options['fields']['field_bm_term']['field'] = 'field_bm_term';
  $handler->display->display_options['fields']['field_bm_term']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_bm_term']['type'] = 'text_plain';
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_bm_email']['id'] = 'field_bm_email';
  $handler->display->display_options['fields']['field_bm_email']['table'] = 'field_data_field_bm_email';
  $handler->display->display_options['fields']['field_bm_email']['field'] = 'field_bm_email';
  $handler->display->display_options['fields']['field_bm_email']['element_wrapper_type'] = 'div';
  /* Field: Content: Twitter */
  $handler->display->display_options['fields']['field_bm_twitter']['id'] = 'field_bm_twitter';
  $handler->display->display_options['fields']['field_bm_twitter']['table'] = 'field_data_field_bm_twitter';
  $handler->display->display_options['fields']['field_bm_twitter']['field'] = 'field_bm_twitter';
  $handler->display->display_options['fields']['field_bm_twitter']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_bm_twitter']['click_sort_column'] = 'url';
  /* Field: Content: Bio */
  $handler->display->display_options['fields']['field_bm_bio']['id'] = 'field_bm_bio';
  $handler->display->display_options['fields']['field_bm_bio']['table'] = 'field_data_field_bm_bio';
  $handler->display->display_options['fields']['field_bm_bio']['field'] = 'field_bm_bio';
  $handler->display->display_options['fields']['field_bm_bio']['label'] = '';
  $handler->display->display_options['fields']['field_bm_bio']['element_label_colon'] = FALSE;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'board_members_draggable:page';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'board_members' => 'board_members',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'board-members';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Board Members';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';
  $export['board_members'] = $view;

  $view = new view();
  $view->name = 'board_members_draggable';
  $view->description = 'This is the view that drives the sort order for the Board members listing.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Board members draggable';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Board members draggable';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Draggableviews: Content */
  $handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['table'] = 'node';
  $handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['label'] = '';
  $handler->display->display_options['fields']['draggableviews']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['save_button_label'] = 'Update';
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
  /* Field: Content: Position */
  $handler->display->display_options['fields']['field_bm_position']['id'] = 'field_bm_position';
  $handler->display->display_options['fields']['field_bm_position']['table'] = 'field_data_field_bm_position';
  $handler->display->display_options['fields']['field_bm_position']['field'] = 'field_bm_position';
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'self';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'board_members' => 'board_members',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/settings/board-members';
  $handler->display->display_options['menu']['title'] = 'Order members';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'Order';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $export['board_members_draggable'] = $view;

  return $export;
}
