<?php
/**
 * @file
 * social_networks.features.inc
 */

/**
 * Implements hook_views_api().
 */
function social_networks_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function social_networks_node_info() {
  $items = array(
    'social_network' => array(
      'name' => t('Social network'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
