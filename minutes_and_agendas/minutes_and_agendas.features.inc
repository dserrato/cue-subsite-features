<?php
/**
 * @file
 * minutes_and_agendas.features.inc
 */

/**
 * Implements hook_views_api().
 */
function minutes_and_agendas_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function minutes_and_agendas_node_info() {
  $items = array(
    'minutes_agenda' => array(
      'name' => t('Minutes & Agenda'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
