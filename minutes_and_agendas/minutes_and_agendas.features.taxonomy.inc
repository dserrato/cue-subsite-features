<?php
/**
 * @file
 * minutes_and_agendas.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function minutes_and_agendas_taxonomy_default_vocabularies() {
  return array(
    'miniutes_agenda_type' => array(
      'name' => 'Minutes Agenda Type',
      'machine_name' => 'miniutes_agenda_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
